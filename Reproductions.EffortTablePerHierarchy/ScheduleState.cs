﻿// This file is part of the MS.Gamification project
// 
// File: ScheduleState.cs  Created: 2017-05-18@16:33
// Last modified: 2017-05-19@02:08

namespace Reproductions.EffortTablePerHierarchy
    {
    public enum ScheduleState
        {
        Scheduled,
        Cancelled,
        InProgress,
        Closed
        }
    }