using System.Data.Common;
using System.Data.Entity;

namespace Reproductions.EffortTablePerHierarchy
    {
    public class ApplicationDbContext : DbContext
        {
        public ApplicationDbContext(string connectionString) : base(connectionString) { }

        public ApplicationDbContext(DbConnection connection) : base(connection, true) { }

        public virtual IDbSet<ObservingSession> ObservingSessions { get; set; }

        public virtual IDbSet<QueuedWorkItem> QueuedWorkItems { get; set; }
        }
    }