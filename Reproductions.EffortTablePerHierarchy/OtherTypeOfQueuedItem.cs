namespace Reproductions.EffortTablePerHierarchy {
    class OtherTypeOfQueuedItem : QueuedWorkItem
        {
        public string Label { get; set; }

        public override string ToString() => $"{base.ToString()}, {nameof(Label)}: {Label}";
        }
    }