﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Effort;
using Effort.Extra;

namespace Reproductions.EffortTablePerHierarchy
    {
    class Program
        {
        static void Main(string[] args)
            {
            var data = LoadData();
            var dbContext = BuildDatabase(data);

            //

            Console.WriteLine($"Observing sessions: {dbContext.ObservingSessions.Count()}");
            Console.WriteLine($"Reminders: {dbContext.QueuedWorkItems.Count()}");

            //

            Console.WriteLine("Reminders in database:");
            var reminders = dbContext.QueuedWorkItems.ToList();
            foreach (var queuedWorkItem in reminders)
                {
                Console.WriteLine(queuedWorkItem);
                }

            //
            Console.ReadLine();
            }

        static ApplicationDbContext BuildDatabase(ObjectData data)
            {
            //ToDo - add data here
            var dataLoader = new ObjectDataLoader(data);
            var DataConnection = DbConnectionFactory.CreateTransient(dataLoader);
            return new ApplicationDbContext(DataConnection);
            }

        static ObjectData LoadData()
            {
            var data = new ObjectData(TableNamingStrategy.Pluralised);
            var startsAt = new DateTime(2000, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            var observingSession = new ObservingSession
                {
                Id = 1,
                Description = "description",
                Title = "fake observing session",
                Venue = "nowhere",
                StartsAt = startsAt,
                RemindOneDayBefore = true,
                RemindOneWeekBefore = true,
                ScheduleState = ScheduleState.Scheduled
                };
            var oneDayReminder = new ObservingSessionReminder
                {
                Id = 1,
                Disposition = WorkItemDisposition.Pending,
                ObservingSessionId = 1,
                ProcessAfter = startsAt - TimeSpan.FromDays(1),
                QueueName = "Events"
                };
            var oneWeekReminder = new ObservingSessionReminder
                {
                Id = 2,
                Disposition = WorkItemDisposition.Pending,
                ObservingSessionId = 1,
                ProcessAfter = startsAt - TimeSpan.FromDays(7),
                QueueName = "Events"
                };
            var otherTypeOfQueuedItem = new OtherTypeOfQueuedItem
                {
                Id = 99,
                ProcessAfter = DateTime.UtcNow,
                QueueName = "Other",
                Disposition = WorkItemDisposition.Pending,
                Label = "This field is only present in OtherTypeOfQueuedItem"
                };

            Console.WriteLine("Creating data loader with the following entities:");
            Console.WriteLine(observingSession);
            Console.WriteLine(oneDayReminder);
            Console.WriteLine(oneWeekReminder);
            Console.WriteLine(otherTypeOfQueuedItem);
            Console.WriteLine();

            // Add entities to the data loader
            data.Table<ObservingSession>().Add(observingSession);
            data.Table<QueuedWorkItem>().Add(oneDayReminder);
            data.Table<QueuedWorkItem>().Add(oneWeekReminder);
            data.Table<QueuedWorkItem>().Add(otherTypeOfQueuedItem);
            return data;
            }
        }
    }
